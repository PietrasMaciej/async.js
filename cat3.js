/*jshint node: true*/
var mongoose = require('mongoose');
var async = require('async');

mongoose.connect('mongodb://localhost/test');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

var catSchema = mongoose.Schema({
    name: String,
    age: Number
});

var Cat = mongoose.model('Cat', catSchema);

var cat1 = new Cat({
    name: 'Rysio',
    age: 1
});
var cat2 = new Cat({
    name: 'Marchewa',
    age: 1
});
var cat3 = new Cat({
    name: 'Dran',
    age: 1
});


var addCat = function (cat) {
  return function (callback) {
    cat.save(function (err) {
        if (err) {
            console.log(err);
        }
        callback(err);
    });
  };
};

var connect = function (callback) {
    db.once('open', function () {
        callback();
    });
};

var insert = function (callback) {
    async.parallel([addCat(cat1), addCat(cat2), addCat(cat3)], function (err) {
        if (err) {
            console.log(err);
        }
        callback();
        process.exit(0);
    });
};

var update = function (callback) {
    async.series([function (done) {
            Cat.findOne({
                name: /^Rysio/
            }, function (err, cat) {
                console.log(cat);
                cat.age = 2;
                cat.save(function (err) {
                    done(err);
                });
            });
    },
    function (done) {
            Cat.update({
                    name: 'Marchewa'
                }, {
                    $set: {
                        age: 2
                    }
                },
                function (err) {
                    done(err);
                }
            );
    },
    function (done) {
            Cat.update({
                    name: 'Dran'
                }, {
                    $inc: {
                        age: 2
                    }
                },
                function (err) {
                    done(err);
                }
            );
    }], function (err) {
        if (err) {
            console.log(err);
        }
        callback();
        process.exit(0);
    });
};

async.series([connect, insert, update]);
