var mongoose = require('mongoose');
var async = require('async');

mongoose.connect('mongodb://localhost/test');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

var catSchema = mongoose.Schema({
    name: String,
    age: Number
});

var Cat = mongoose.model('Cat', catSchema);

var cat1 = new Cat({ name: 'Rysio', age: 1 });
var cat2 = new Cat({ name: 'Marchewa', age: 1 });
var cat3 = new Cat({ name: 'Drań', age: 1 });


  var connect = function (cb) {
    db.once('open', function () {
        cb();
    });
  };

  var insert = function (cb) {
            async.parallel([

                          function (catSaveDone) {
                              cat1.save(function (err) {
                                  if (err) {
                                      console.log(err);
                                  }
                                  catSaveDone(err);
                              });
                          },
                          function (catSaveDone) {
                              cat2.save(function (err, kitty) {
                                  if (err) {
                                      console.log(err);
                                      catSaveDone(err);
                                  }
                                  console.log(kitty);
                                  catSaveDone();
                              });
                          },
                          function (catSaveDone) {
                              cat3.save(function (err, kitty) {
                                  if (err) {
                                      console.log(err);
                                  }
                                  catSaveDone(err);
                              });
                          }
                        ],

    function (err) { if (err) { console.log(err);  cb(err);}  cb();   process.exit(0);  }

  );};

  var update = function (cb) {
    async.series([
    function (done) {
        Cat.findOne({ name: /^Rysio/ }, function (err, cat) {
            console.log(cat);
            cat.age = 2;
            cat.save(function (err) {
                done(err);
            });
        });
    },
    function (done) {
        Cat.update({name: 'Marchewa'}, {$set: {age: 2}},
            function (err) {
                done(err);
            }
        );
    },
    function (done) {
        Cat.update({name: 'Drań'}, {$inc: {age: 2}},
            function (err) {
                done(err);
            }
        );
    }],
    function (err) {
          if (err) {
              console.log(err);
              cb(err);
          }
  cb();
          process.exit(0);

      });

  };


  async.series([connect, insert, update]);
